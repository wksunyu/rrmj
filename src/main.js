// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import adapter from "./common/js/adapter"
import wxshare from "./common/js/zhuanti1.0"
import wkshare from "./common/js/wkshare"
Vue.config.productionTip = false;
Vue.prototype.wkurl = 'https://api.wukongtv.com/';
// Vue.prototype.wkurl = 'http://test.api.wukongtv.com:30008/';
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
