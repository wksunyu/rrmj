import Vue from 'vue'
import App from '../App'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import homepage from '../components/homepage/homepage.vue'
import videoslist from '../components/videoslist/videoslist.vue'
import videodetail from '../components/videodetail/videodetail.vue'
import talent from  '../components/talent/talent.vue'
import baseview from '../components/baseview.vue'
import synopsis from '../components/synopsis/synopsis.vue'
import yunshare from '../components/videodetail/baidunshare.vue'
Vue.use(VueRouter);
Vue.use(VueResource);
const router = new VueRouter({
  linkActiveClass: 'active',
  routes: [
    {
      path: '/',
      component: App,
      children: [
        {
          path: '',
          name:'Home',
          component: homepage
        },
        {
          path: '/updated/:id',
          name:'Updated',
          component: videoslist
        },
        {
          path: '/terminate',
          name:'Terminate',
          component: videoslist
        },
        {
          path: '/videodetail/:id',
          name:'Videodetail',
          component: videodetail
        },
        {
          path: '/videodetail/:id/:from',
          name:'ShareVideodetail',
          component: videodetail
        },
        {
          path: '/search',
          name:'Search',
          component: baseview,
          beforeEnter: (to, from, next) => {
              window.location.href = 'http://m.panduoduo.net/';
              sendCNZZ('云搜索','浏览')
          }
        },
        {
          path: '/QP',
          name:'QP',
          component: baseview,
          beforeEnter: (to, from, next) => {
            window.location.href = 'https://www.wenjuan.com/s/zaErua/';
            sendCNZZ('帮你找片','浏览')
          }
        },
        {
          path: '/talent',
          name:'Talent',
          component: talent
        },
        {
          path: '/wkbdy',
          name:'Synopsis',
          component: synopsis
        },
        {
          path: '/yunshare',
          name:'Yunshare',
          component: yunshare
        }
      ]
    }
  ]
});
export  default router
